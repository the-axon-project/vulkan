// Vulkan graphics and compute API
// Copyright (C) 2019 Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Code Generation

use crate::types::*;
use log::trace;
use std::fmt::Write;

pub(crate) fn generate(registry: Registry<'_>) -> String {
    trace!("BEGIN generate");

    let mut output = String::default();
    writeln!(&mut output, "{}", COPYRIGHT).unwrap();
    writeln!(&mut output, "{}", HEADER).unwrap();
    writeln!(&mut output, "{}", DEFINES).unwrap();

    let feature_v_1_0 = registry.features.iter().find(|x| x.number == "1.0").unwrap();
    let feature_v_1_1 = registry.features.iter().find(|x| x.number == "1.1").unwrap();

    let dependencies_v_1_0 = calculate_dependency_tree(&registry.types, feature_v_1_0);
    let dependencies_v_1_1 = calculate_dependency_tree(&registry.types, feature_v_1_1);

    let mut dependencies = Vec::new();
    dependencies.extend_from_slice(&dependencies_v_1_0);
    dependencies.extend_from_slice(&dependencies_v_1_1);
    dependencies.retain(|&x| x != "");
    dependencies.sort();
    dependencies.dedup();

    let recursive_dependencies = calculate_recursive_dependency_tree(&registry.types, dependencies);

    codegen_commands(&mut output, &registry.types, &recursive_dependencies);
    codegen_structs(&mut output, &registry.types, &recursive_dependencies);
    codegen_enumerants(&mut output, &registry.types, &recursive_dependencies);
    codegen_basetypes(&mut output, &registry.types, &recursive_dependencies);
    codegen_handles(&mut output, &registry.types, &recursive_dependencies);
    codegen_funcpointers(&mut output, &registry.types, &recursive_dependencies);
    codegen_bitflags(&mut output, &registry.types, &recursive_dependencies);
    codegen_unions(&mut output, &registry.types, &recursive_dependencies);
    codegen_constants(&mut output, &registry.types, &recursive_dependencies);

    trace!("END generate");
    output
}

const COPYRIGHT: &str = r"
//! Vulkan API (generated)
//!
//! See https://gitlab.com/the-axon-project/vulkan/ for details.
//!
//! Copyright (C) 2019 Andrew Green <tuxsoy@protonmail.com>
//!
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the GNU Affero General Public License as
//! published by the Free Software Foundation, either version 3 of the
//! License, or (at your option) any later version.
//!
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU Affero General Public License for more details.
//!
//! You should have received a copy of the GNU Affero General Public License
//! along with this program.  If not, see <https://www.gnu.org/licenses/>.
";

const HEADER: &str = r#"
#![feature(extern_types)]
#![feature(untagged_unions)]
#![feature(const_generics)]

#![forbid(box_pointers)]
#![forbid(elided_lifetimes_in_paths)]
#![forbid(future_incompatible)]
#![forbid(missing_copy_implementations)]
#![forbid(rust_2018_idioms)]
#![forbid(single_use_lifetimes)]
#![forbid(trivial_casts)]
#![forbid(trivial_numeric_casts)]
#![forbid(unused_import_braces)]
#![forbid(unused_results)]
#![forbid(variant_size_differences)]

#![deny(unused)]
#![deny(unused_qualifications)]
#![deny(warnings)]

#![allow(missing_debug_implementations)]
#![allow(missing_docs)]
#![allow(nonstandard_style)]
#![allow(unused_imports)]
#![allow(incomplete_features)]

use std::ffi::c_void;
use std::os::raw::{c_char, c_int};
use std::ptr::NonNull;
use std::fmt;
use std::cmp::Ordering;
"#;

const DEFINES: &str = r#"
pub const fn VK_MAKE_VERSION(major: u32, minor: u32, patch: u32) -> u32 {
    (major << 22) | (minor << 12) | patch
}

pub const fn VK_VERSION_MAJOR(version: u32) -> u32 {
    version >> 22
}

pub const fn VK_VERSION_MINOR(version: u32) -> u32 {
    (version >> 12) & 0x3FF
}

pub const fn VK_VERSION_PATCH(version: u32) -> u32 {
    version & 0xFFF
}

pub const VK_API_VERSION_1_0: u32 = VK_MAKE_VERSION(1, 0, 0);
pub const VK_API_VERSION_1_1: u32 = VK_MAKE_VERSION(1, 1, 0);
pub const VK_HEADER_VERSION: u32 = 97;

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct Array<T, const N: usize>(pub [T; N]);

impl<T: fmt::Debug, const N: usize> fmt::Debug for Array<T, {N}> {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0[..].fmt(formatter)
    }
}

impl<T: PartialEq, const N: usize> PartialEq for Array<T, {N}> {
    fn eq(&self, other: &Array<T, {N}>) -> bool {
        self.0.iter().zip(other.0.iter()).all(|(a,b)| *a == *b)
    }
    fn ne(&self, other: &Array<T, {N}>) -> bool {
        self.0.iter().zip(other.0.iter()).all(|(a,b)| *a != *b)
    }
}

impl<T: PartialOrd, const N: usize> PartialOrd for Array<T, {N}> {
    fn partial_cmp(&self, other: &Array<T, {N}>) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<T: Ord, const N: usize> Ord for Array<T, {N}> {
    fn cmp(&self, other: &Array<T, {N}>) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl<T: Eq, const N: usize> Eq for Array<T, {N}> {}
"#;

fn calculate_derive_helper(ctype: &str) -> &str {
    let trueable = "VkBool32";
    let falsable = "VkStructureType";
    if ctype.trim().ends_with("*") {
        return trueable
    } else if ctype.trim().starts_with("const ") {
        return calculate_derive_helper(ctype.trim_start_matches("const ").trim())
    } else if ctype.trim().starts_with("struct ") {
        return calculate_derive_helper(ctype.trim_start_matches("struct ").trim())
    } else if ctype.trim().contains("[") {
        return falsable
    }
    match ctype {
        "char" => trueable,
        "float" => trueable,
        "int32_t" => trueable,
        "size_t" => trueable,
        "uint32_t" => trueable,
        "uint64_t" => trueable,
        "void" => trueable,
        "uint8_t" => trueable,
        _ => ctype,
    }
}

fn calculate_default(types: &Vec<Type<'_>>, ctype: &str) -> bool {
    for t in types {
        match t {
            Type::Basetype { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Alias { name, r#type, } => {
                if *name == ctype {
                    return calculate_default(types, calculate_derive_helper(r#type))
                }
            }
            Type::Bitmask { name, .. } => {
                if *name == ctype {
                    return false
                }
            }
            Type::Handle { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::FunctionPointer { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Struct { name, members, .. } => {
                if *name == ctype {
                    return members.iter().all(|x| calculate_default(types, calculate_derive_helper(x.r#type.as_ref())))
                }
            }
            Type::Union { name, .. } => {
                if *name == ctype {
                    return false
                }
            }
            Type::Constant { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Enumerant { name, .. } => {
                if *name == ctype {
                    return false
                }
            }
            Type::Command { name, .. } => {
                if *name == ctype {
                    panic!("struct fields shouldn't be commands?")
                }
            }
        }
    }
    panic!("could not find type: {}", ctype)
}

fn calculate_union(types: &Vec<Type<'_>>, ctype: &str) -> bool {
    for t in types {
        match t {
            Type::Basetype { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Alias { name, r#type, } => {
                if *name == ctype {
                    return calculate_union(types, calculate_derive_helper(r#type))
                }
            }
            Type::Bitmask { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Handle { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::FunctionPointer { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Struct { name, members, .. } => {
                if *name == ctype {
                    return members.iter().all(|x| calculate_union(types, calculate_derive_helper(x.r#type.as_ref())))
                }
            }
            Type::Union { name, .. } => {
                if *name == ctype {
                    return false
                }
            }
            Type::Constant { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Enumerant { name, .. } => {
                if *name == ctype {
                    return true
                }
            }
            Type::Command { name, .. } => {
                if *name == ctype {
                    panic!("struct fields shouldn't be commands?")
                }
            }
        }
    }
    panic!("could not find type: {}", ctype)
}

fn codegen_commands(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    writeln!(output, "#[link(name = \"vulkan\")]").unwrap();
    writeln!(output, "extern \"C\" {{").unwrap();
    for t in types {
        match t {
            Type::Command {
                name,
                r#type,
                parameters,
                ..
            } => {
                if names.contains(name) {
                    write!(output, "pub fn {}(", name).unwrap();
                    for parameter in parameters {
                        let cname = format!("r#{}", parameter.name);
                        let ctype = generate_ctype(parameter.r#type.as_ref());
                        write!(output, "{}: {}, ", cname, ctype).unwrap();
                    }
                    write!(output, ")").unwrap();
                    match r#type.trim().as_ref() {
                        "void" => writeln!(output, ";").unwrap(),
                        _ => writeln!(output, " -> {};", r#type.trim()).unwrap(),
                    }
                    writeln!(output).unwrap();
                }
            }
            _ => {}
        }
    }
    writeln!(output, "}}").unwrap();
    writeln!(output).unwrap();
}

fn codegen_basetypes(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::Basetype { name, r#type } => {
                if names.contains(name) {
                    let ctype = generate_ctype(r#type);
                    writeln!(output, "#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]").unwrap();
                    writeln!(output, "#[repr(transparent)]").unwrap();
                    writeln!(output, "pub struct {}(pub {});", name, ctype).unwrap();
                    writeln!(output).unwrap();
                }
            }
            _ => {}
        }
    }
}

fn codegen_funcpointers(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::FunctionPointer {
                name,
                r#type,
                ..
            } => {
                if names.contains(name) {
                    let ctype = match generate_ctype(r#type).as_ref() {
                        "c_void" => String::new(),
                        x => format!(" -> {}", x),
                    };
                    writeln!(
                        output,
                        "pub type {} = Option<extern \"C\" fn(){}>;",
                        name, ctype
                    )
                    .unwrap();
                }
            }
            _ => {}
        }
    }
}

fn codegen_enumerants(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::Enumerant {
                name,
                values,
                ..
            } => {
                if names.contains(name) {
                    writeln!(output, "#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]").unwrap();
                    writeln!(output, "#[repr(C)]").unwrap();
                    writeln!(output, "pub enum {} {{", name).unwrap();
                    for value in values {
                        writeln!(output, "{}={},", value.name, value.value).unwrap();
                    }
                    writeln!(output, "}}").unwrap();
                    writeln!(output).unwrap();
                }
            }
            _ => {}
        }
    }
}

fn codegen_structs(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::Struct {
                name,
                members,
                ..
            } => {
                if names.contains(name) {
                    let defaultstr = if calculate_default(types, name) {
                        ", Default"
                    } else {
                        ""
                    };
                    let unionstr = if calculate_union(types, name) {
                        ", PartialEq, PartialOrd"
                    } else {
                        ""
                    };
                    writeln!(output, "#[derive(Debug, Clone, Copy{}{})]", defaultstr, unionstr).unwrap();
                    writeln!(output, "#[repr(C)]").unwrap();
                    writeln!(output, "pub struct {} {{", name).unwrap();
                    for member in members {
                        let cname = format!("r#{}", member.name);
                        let ctype = generate_ctype(member.r#type.as_ref());
                        writeln!(output, "{}: {},", cname, ctype).unwrap();
                    }
                    writeln!(output, "}}").unwrap();
                    writeln!(output).unwrap();

                    writeln!(output, "#[derive(Debug, Clone, Copy, Default{})]", unionstr).unwrap();
                    writeln!(output, "#[repr(C)]").unwrap();
                    writeln!(output, "pub struct {}Builder {{", name).unwrap();
                    for member in members {
                        let cname = format!("r#{}", member.name);
                        let ctype = generate_ctype(member.r#type.as_ref());
                        writeln!(output, "{}: Option<{}>,", cname, ctype).unwrap();
                    }
                    writeln!(output, "}}").unwrap();
                    writeln!(output).unwrap();

                    writeln!(output, "impl {} {{", name).unwrap();
                    writeln!(output, "pub fn build() -> {}Builder {{", name).unwrap();
                    writeln!(output, "{}Builder {{", name).unwrap();
                    for member in members {
                        let cname = format!("r#{}", member.name);
                        writeln!(output, "{}: None,", cname).unwrap();
                    }
                    writeln!(output, "}}").unwrap();
                    writeln!(output, "}}").unwrap();
                    writeln!(output, "}}").unwrap();
                    writeln!(output).unwrap();

                    writeln!(output, "impl {}Builder {{", name).unwrap();

                    for member in members {
                        let cname = format!("r#{}", member.name);
                        let ctype = generate_ctype(member.r#type.as_ref());
                        writeln!(output, "pub fn {}(self, value: {}) -> Self {{", cname, ctype).unwrap();
                        writeln!(output, "{}Builder {{", name).unwrap();
                        for m in members {
                            let cname = format!("r#{}", m.name);
                            if member == m {
                                writeln!(output, "{}: Some(value),", cname).unwrap();
                            } else {
                                writeln!(output, "{}: self.{},", cname, cname).unwrap();
                            }
                        }
                        writeln!(output, "}}").unwrap();
                        writeln!(output, "}}").unwrap();
                    }

                    writeln!(output, "pub fn finish(self) -> {} {{", name).unwrap();
                    writeln!(output, "{} {{", name).unwrap();
                    for member in members {
                        let cname = format!("r#{}", member.name);
                        writeln!(output, "{}: self.{}.unwrap(),", cname, cname).unwrap();
                    }
                    writeln!(output, "}}").unwrap();
                    writeln!(output, "}}").unwrap();
                    writeln!(output, "}}").unwrap();
                    writeln!(output).unwrap();
                }
            }
            _ => {}
        }
    }
}

fn generate_ctype(input: &str) -> String {
    if input.starts_with("const ") {
        return generate_ctype(input.trim_start_matches("const "))
    } else if input.ends_with("*") {
        return format!("Option<NonNull<{}>>", generate_ctype(input.trim_end_matches("*").trim()))
    } else if input.starts_with("struct ") {
        return generate_ctype(input.trim_start_matches("struct "))
    } else if input.ends_with(" const") {
        return generate_ctype(input.trim_end_matches(" const"))
    } else if input.contains("[") && input.contains("]") {
        let size_start = input.find("[").unwrap();
        let size_end = input.find("]").unwrap();
        let ctype = &input[..size_start];
        let csize = &input[size_start+1..size_end];
        // return format!("Array([{}; {}])", generate_ctype(ctype), csize)
        return format!("Array<{}, {{{}}}>", generate_ctype(ctype), csize)
    }

    match input.trim() {
        "void" => String::from("c_void"),
        "char" => String::from("c_char"),
        "float" => String::from("f32"),
        "uint8_t" => String::from("u8"),
        "uint16_t" => String::from("u16"),
        "uint32_t" => String::from("u32"),
        "uint64_t" => String::from("u64"),
        "int32_t" => String::from("i32"),
        "size_t" => String::from("usize"),
        "int" => String::from("c_int"),

        x => String::from(x),
    }
}

fn calculate_dependency_tree<'a>(types: &'a Vec<Type<'_>>, feature: &Feature<'a>) -> Vec<&'a str> {
    let mut output = Vec::new();
    let blacklist = ["vk_platform", "VK_API_VERSION", "VK_API_VERSION_1_0", "VK_VERSION_MAJOR", "VK_VERSION_MINOR", "VK_VERSION_PATCH", "VK_HEADER_VERSION", "VK_API_VERSION_1_1"];
    for require in &feature.requires {
        match require {
            FeatureRequire::Command { name } => {
                if !blacklist.contains(name) {
                    output.push(*name);
                    output.append(&mut calculate_command_dependency_tree(types, name));
                }
            }
            FeatureRequire::Type { name } => {
                if !blacklist.contains(name) {
                    output.push(*name);
                }
            }
            FeatureRequire::Enum { name } => {
                if !blacklist.contains(name) {
                    output.push(*name);
                }
            }
        }
    }
    output
}

fn calculate_command_dependency_tree<'a>(types: &'a Vec<Type<'_>>, cname: &str) -> Vec<&'a str> {
    let mut output = Vec::new();
    for t in types {
        match t {
            Type::Command {
                name,
                r#type,
                parameters,
                ..
            } => {
                if cname == *name {
                    output.append(&mut generate_ctype_dependency(r#type));
                    for parameter in parameters {
                        output.append(&mut generate_ctype_dependency(parameter.r#type.as_ref()));
                    }
                }
            }
            _ => {}
        }
    }
    output
}

fn generate_ctype_dependency(input: &str) -> Vec<&str> {
    let mut output = Vec::new();
    if input.starts_with("const ") {
        return generate_ctype_dependency(input.trim_start_matches("const "))
    } else if input.ends_with("*") {
        return generate_ctype_dependency(input.trim_end_matches("*").trim())
    } else if input.starts_with("struct ") {
        return generate_ctype_dependency(input.trim_start_matches("struct "))
    } else if input.contains("[") {
        let csize = input.splitn(2, "[").nth(1).unwrap().rsplitn(2, "]").nth(1).unwrap().trim();
        output.push(csize);
        output.append(&mut generate_ctype_dependency(input.splitn(2, "[").nth(0).unwrap().trim()));
        return output
    }

    match input.trim() {
        "char" => {},
        "float" => {},
        "int32_t" => {},
        "size_t" => {},
        "uint32_t" => {},
        "uint64_t" => {},
        "void" => {},
        "uint8_t" => {},
        "char * const" => {},

        x => output.push(x),
    }
    output
}

fn calculate_recursive_dependency_tree<'a>(types: &'a Vec<Type<'_>>, names: Vec<&'a str>) -> Vec<&'a str> {
    let mut output = Vec::new();
    for t in types {
        match t {
            Type::Basetype { .. } => {}
            Type::Alias {
                name,
                r#type,
            } => {
                if names.contains(name) {
                    output.append(&mut generate_ctype_dependency(r#type));
                }
            }
            Type::Bitmask {
                name,
                r#type,
                ..
            } => {
                if names.contains(name) {
                    output.push(r#type);
                }
            }
            Type::Handle {
                name,
                ..
            } => {
                if names.contains(name) {
                    output.push(name);
                }
            }
            Type::FunctionPointer {
                name,
                r#type,
                arguments,
            } => {
                if names.contains(name) {
                    output.append(&mut generate_ctype_dependency(r#type));
                    for argument in arguments {
                        output.append(&mut generate_ctype_dependency(argument.r#type.as_ref()));
                    }
                }
            }
            Type::Struct {
                name,
                members,
                ..
            } => {
                if names.contains(name) {
                    for member in members {
                        output.append(&mut generate_ctype_dependency(member.r#type.as_ref()));
                    }
                }
            }
            Type::Union {
                name,
                members,
                ..
            } => {
                if names.contains(name) {
                    for member in members {
                        output.append(&mut generate_ctype_dependency(member.r#type.as_ref()));
                    }
                }
            }
            Type::Constant {
                name,
                ..
            } => {
                if names.contains(name) {
                    output.push(name);
                }
            }
            Type::Enumerant {
                name,
                ..
            } => {
                if names.contains(name) {
                    output.push(name);
                }
            }
            Type::Command {
                name,
                parameters,
                ..
            } => {
                if names.contains(name) {
                    for parameter in parameters {
                        output.append(&mut generate_ctype_dependency(parameter.r#type.as_ref()));
                    }
                }
            }
        }
    }

    output.extend_from_slice(&names);
    output.retain(|&x| x != "");
    output.sort();
    output.dedup();

    if output.len() == names.len() {
        output
    } else {
        calculate_recursive_dependency_tree(types, output)
    }
}

fn codegen_handles(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::Handle { name, r#type, .. } => {
                if names.contains(name) {
                    match r#type {
                        HandleType::Dispatchable => {
                            writeln!(output, "extern \"C\" {{").unwrap();
                            writeln!(output, "pub type {}_T;", name).unwrap();
                            writeln!(output, "}}").unwrap();
                            writeln!(output).unwrap();
                            writeln!(output, "#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]").unwrap();
                            writeln!(output, "#[repr(transparent)]").unwrap();
                            writeln!(output, "pub struct {}(pub Option<NonNull<{}_T>>);", name, name).unwrap();
                            writeln!(output).unwrap();
                        }
                        HandleType::NonDispatchable => {
                            writeln!(output, "#[cfg(target_pointer_width = \"64\")]").unwrap();
                            writeln!(output, "extern \"C\" {{").unwrap();
                            writeln!(output, "pub type {}_T;", name).unwrap();
                            writeln!(output, "}}").unwrap();
                            writeln!(output, "#[cfg(target_pointer_width = \"64\")]").unwrap();
                            writeln!(output, "#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]").unwrap();
                            writeln!(output, "#[repr(transparent)]").unwrap();
                            writeln!(output, "pub struct {}(pub Option<NonNull<{}_T>>);", name, name).unwrap();
                            writeln!(output).unwrap();
                            writeln!(output, "#[cfg(not(target_pointer_width = \"64\"))]").unwrap();
                            writeln!(output, "#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]").unwrap();
                            writeln!(output, "#[repr(transparent)]").unwrap();
                            writeln!(output, "pub struct {}(pub u64);", name).unwrap();
                            writeln!(output).unwrap();
                        }
                    }
                }
            }
            _ => {}
        }
    }
}

fn codegen_bitflags(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::Bitmask { name, r#type, requires } => {
                if names.contains(name) {
                    writeln!(output, "#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]").unwrap();
                    writeln!(output, "#[repr(transparent)]").unwrap();
                    writeln!(output, "pub struct {}(pub {});", name, r#type).unwrap();
                    writeln!(output).unwrap();
                    if let Some(require) = requires {
                        writeln!(output, "impl {} {{", name).unwrap();
                            let bname = name;
                            let btype = r#type;
                            for t in types {
                                match t {
                                    Type::Enumerant { name, values, .. } => {
                                        for value in values {
                                            if require == name {
                                                writeln!(output, "pub const {}: Self = {}({}({}));", value.name, bname, btype, value.value).unwrap();
                                                writeln!(output).unwrap();
                                            }
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        writeln!(output, "}}").unwrap();
                        writeln!(output).unwrap();
                    }
                }
            }
            _ => {}
        }
    }
}

fn codegen_unions(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::Union { name, members, .. } => {
                if names.contains(name) {
                    writeln!(output, "#[derive(Clone, Copy)]").unwrap();
                    writeln!(output, "#[repr(C)]").unwrap();
                    writeln!(output, "pub union {} {{", name).unwrap();
                    for member in members {
                        writeln!(output, "{}: {},", member.name, generate_ctype(member.r#type.as_ref())).unwrap();
                        writeln!(output).unwrap();
                    }
                    writeln!(output, "}}").unwrap();
                    writeln!(output).unwrap();
                    writeln!(output, "impl fmt::Debug for {} {{", name).unwrap();
                    writeln!(output, "fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {{").unwrap();
                    writeln!(output, "write!(f, \"{} {{{{ ... }}}}\")", name).unwrap();
                    writeln!(output, "}}").unwrap();
                    writeln!(output, "}}").unwrap();
                    writeln!(output).unwrap();
                }
            }
            _ => {}
        }
    }
}

fn codegen_constants(output: &mut String, types: &Vec<Type<'_>>, names: &Vec<&str>) {
    for t in types {
        match t {
            Type::Constant { name, value, .. } => {
                if names.contains(name) {
                    if *name == "VK_TRUE" || *name == "VK_FALSE" {
                        writeln!(output, "pub const {}: VkBool32 = VkBool32({});", name, value).unwrap();
                    } else if *value == "(~0U)" {
                        writeln!(output, "pub const {}: u32 = !0;", name).unwrap();
                    } else if *value == "(~0ULL)" {
                        writeln!(output, "pub const {}: u64 = !0;", name).unwrap();
                    } else if value.ends_with("f") {
                        let value = value.trim_end_matches("f");
                        writeln!(output, "pub const {}: f32 = {};", name, value).unwrap();
                    } else if *value == "(~0U-1)" {
                        writeln!(output, "pub const {}: u32 = !0 - 1;", name).unwrap();
                    } else if *value == "(~0U-2)" {
                        writeln!(output, "pub const {}: u32 = !0 - 2;", name).unwrap();
                    } else {
                        writeln!(output, "pub const {}: usize = {};", name, value).unwrap();
                    }
                }
            }
            _ => {}
        }
    }
}
