// Vulkan graphics and compute API
// Copyright (C) 2019 Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Registry<'a> {
    pub(crate) platforms: Vec<Platform<'a>>,
    pub(crate) tags: Vec<Tag<'a>>,
    pub(crate) types: Vec<Type<'a>>,
    pub(crate) features: Vec<Feature<'a>>,
    pub(crate) extensions: Vec<Extension<'a>>,
}

/// Vulkan platform names, reserved for use with platform- and window system-specific extensions
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Platform<'a> {
    pub(crate) name: &'a str,
    pub(crate) protect: &'a str,
    pub(crate) comment: &'a str,
}

/// Vulkan vendor/author tags for extensions and layers
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Tag<'a> {
    pub(crate) name: &'a str,
    pub(crate) author: &'a str,
    pub(crate) contact: &'a str,
}

/// Vulkan type definitions
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) enum Type<'a> {
    Basetype {
        name: &'a str,
        r#type: &'a str,
    },
    Alias {
        name: &'a str,
        r#type: &'a str,
    },
    Bitmask {
        name: &'a str,
        r#type: &'a str,
        requires: Option<&'a str>,
    },
    Handle {
        name: &'a str,
        r#type: HandleType,
        parent: Option<&'a str>,
    },
    FunctionPointer {
        name: &'a str,
        r#type: &'a str,
        arguments: Vec<FunctionArgument>,
    },
    Struct {
        name: &'a str,
        members: Vec<StructMember<'a>>,
        returnedonly: bool,
        structextends: Option<&'a str>,
    },
    Union {
        name: &'a str,
        members: Vec<UnionMember<'a>>,
        comment: &'a str,
    },
    Constant {
        name: &'a str,
        value: &'a str,
        comment: Option<&'a str>,
    },
    Enumerant {
        name: &'a str,
        r#type: &'a str,
        values: Vec<EnumerantValue<'a>>,
    },
    Command {
        name: &'a str,
        r#type: String,
        successcodes: Vec<&'a str>,
        errorcodes: Vec<&'a str>,
        parameters: Vec<CommandParameter<'a>>,
    },
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct EnumerantValue<'a> {
    pub(crate) name: &'a str,
    pub(crate) value: isize,
    pub(crate) comment: Option<&'a str>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Feature<'a> {
    pub(crate) number: &'a str,
    pub(crate) comment: Option<&'a str>,
    pub(crate) requires: Vec<FeatureRequire<'a>>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) enum FeatureRequire<'a> {
    Command { name: &'a str },
    Type { name: &'a str },
    Enum { name: &'a str },
}

/// Vulkan extension interface definitions
#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Extension<'a> {
    pub(crate) comment: &'a str,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct StructMember<'a> {
    pub(crate) name: &'a str,
    pub(crate) r#type: String,
    pub(crate) comment: Option<&'a str>,
    pub(crate) optional: bool,
    pub(crate) noautovalidity: bool,
    pub(crate) len: Option<&'a str>,
    pub(crate) values: Option<&'a str>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct UnionMember<'a> {
    pub(crate) name: &'a str,
    pub(crate) r#type: String,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct CommandParameter<'a> {
    pub(crate) name: &'a str,
    pub(crate) r#type: String,
    pub(crate) optional: bool,
    pub(crate) externsync: Option<&'a str>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct FunctionArgument {
    pub(crate) name: String,
    pub(crate) r#type: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) enum HandleType {
    Dispatchable,
    NonDispatchable,
}
