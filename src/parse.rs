// Vulkan graphics and compute API
// Copyright (C) 2019 Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Parse

use crate::types::*;
use log::trace;
use roxmltree::Node;

pub(crate) fn parse<'input>(node: Node<'input, 'input>) -> Registry<'input> {
    trace!("BEGIN parse");

    assert!(node.has_tag_name("registry"));

    let mut registry = Registry::default();

    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "platforms" => registry.platforms.append(&mut parse_platforms(child)),
            "tags" => registry.tags.append(&mut parse_tags(child)),
            "types" => registry.types.append(&mut parse_types(child)),
            "enums" => {
                if child.attribute("name").unwrap() == "API Constants" {
                    registry.types.append(&mut parse_constants(child));
                } else {
                    registry.types.append(&mut parse_enums(child));
                }
            }
            "commands" => registry.types.append(&mut parse_commands(child)),
            "feature" => registry.features.push(parse_feature(child)),
            "extensions" => parse_extensions(child),
            _ => panic!("unimplemented registry feature"),
        }
    }

    trace!("END parse");

    registry
}

fn parse_platforms<'input>(node: Node<'input, 'input>) -> Vec<Platform<'input>> {
    trace!("BEGIN parse_platforms");

    let mut platforms = Vec::new();

    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "platform" => {
                assert!(child.has_attribute("name"));
                assert!(child.has_attribute("protect"));
                assert!(child.has_attribute("comment"));

                let platform = Platform {
                    name: child.attribute("name").unwrap(),
                    protect: child.attribute("protect").unwrap(),
                    comment: child.attribute("comment").unwrap(),
                };

                platforms.push(platform);
            }
            _ => panic!("unimplemented platform"),
        }
    }

    trace!("END parse_platforms");

    platforms
}

fn parse_tags<'input>(node: Node<'input, 'input>) -> Vec<Tag<'input>> {
    trace!("BEGIN parse_tags");

    let mut tags = Vec::new();

    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "tag" => {
                assert!(child.has_attribute("name"));
                assert!(child.has_attribute("author"));
                assert!(child.has_attribute("contact"));

                let tag = Tag {
                    name: child.attribute("name").unwrap(),
                    author: child.attribute("author").unwrap(),
                    contact: child.attribute("contact").unwrap(),
                };

                tags.push(tag);
            }
            _ => panic!("unimplemented tag"),
        }
    }

    trace!("END parse_tags");

    tags
}

fn parse_types<'input>(node: Node<'input, 'input>) -> Vec<Type<'input>> {
    trace!("BEGIN parse_types");

    let mut types = Vec::new();

    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "type" => {
                let inner_name = child.children().find(|x| x.tag_name().name() == "name");
                let property_a = child.attribute("name").is_some();
                let property_b = inner_name.is_some() && inner_name.unwrap().text().is_some();
                assert!(property_a || property_b);
                let name = child
                    .attribute("name")
                    .unwrap_or_else(|| inner_name.unwrap().text().unwrap());

                if let Some(category) = child.attribute("category") {
                    match category {
                        "include" => {
                            // XXX: Translate manually
                        }
                        "define" => {
                            // XXX: Translate manually
                        }
                        "basetype" => {
                            let inner_type =
                                child.children().find(|x| x.tag_name().name() == "type");
                            assert!(inner_type.is_some() && inner_type.unwrap().text().is_some());
                            let basetype = Type::Basetype {
                                name,
                                r#type: inner_type.unwrap().text().unwrap(),
                            };
                            types.push(basetype);
                        }
                        "bitmask" => {
                            if let Some(alias) = child.attribute("alias") {
                                assert!(child.attribute("alias").is_some());
                                let alias = Type::Alias {
                                    name,
                                    r#type: alias,
                                };
                                types.push(alias);
                            } else {
                                let inner_type =
                                    child.children().find(|x| x.tag_name().name() == "type");
                                assert!(inner_type.is_some() && inner_type.unwrap().text().is_some());
                                let bitmask = Type::Bitmask {
                                    name,
                                    r#type: inner_type.unwrap().text().unwrap(),
                                    requires: child.attribute("requires"),
                                };
                                types.push(bitmask);
                            }
                        }
                        "handle" => {
                            if let Some(r#type) = child.attribute("alias") {
                                let alias = Type::Alias {
                                    name,
                                    r#type,
                                };
                                types.push(alias);
                            } else {
                                let r#type =
                                    child.children().find(|x| x.tag_name().name() == "type");
                                assert!(r#type.is_some() && r#type.unwrap().text().is_some());
                                let r#type = match r#type.unwrap().text().unwrap() {
                                    "VK_DEFINE_HANDLE" => HandleType::Dispatchable,
                                    "VK_DEFINE_NON_DISPATCHABLE_HANDLE" => {
                                        HandleType::NonDispatchable
                                    }
                                    _ => panic!("unimplemented handle type"),
                                };
                                let parent = child.attribute("parent");
                                let handle = Type::Handle {
                                    name,
                                    r#type,
                                    parent,
                                };
                                types.push(handle);
                            }
                        }
                        "enum" => {
                            if let Some(alias) = child.attribute("alias") {
                                let alias = Type::Alias {
                                    name,
                                    r#type: alias,
                                };
                                types.push(alias);
                            }
                        }
                        "funcpointer" => {
                            let mut arguments = Vec::new();

                            let rawstring: String = child
                                .children()
                                .filter_map(|x| x.text())
                                .flat_map(|x| x.chars())
                                .collect();
                            let r#type = child
                                .children()
                                .next()
                                .unwrap()
                                .text()
                                .unwrap()
                                .splitn(2, "typedef ")
                                .nth(1)
                                .unwrap()
                                .splitn(2, " (VKAPI_PTR *")
                                .nth(0)
                                .unwrap();

                            for line in rawstring.lines().skip(1) {
                                let r#type =
                                    line.trim().splitn(2, "  ").nth(0).unwrap().to_string();
                                let name = line
                                    .trim()
                                    .splitn(2, "  ")
                                    .nth(0)
                                    .unwrap()
                                    .trim()
                                    .trim_end_matches(',')
                                    .trim_end_matches(");")
                                    .to_string();
                                let arg = FunctionArgument { name, r#type };
                                arguments.push(arg);
                            }

                            let funcpointer = Type::FunctionPointer {
                                name,
                                r#type,
                                arguments,
                            };

                            types.push(funcpointer);
                        }
                        "struct" => {
                            let mut members = Vec::new();
                            for grandchild in child.children() {
                                if grandchild.children().count() < 2 {
                                    continue;
                                }

                                let r#type = {
                                    let mut start: Vec<&str> = grandchild
                                        .children()
                                        .take_while(|p| p.tag_name().name() != "name")
                                        .filter(|p| p.text().is_some())
                                        .map(|p| p.text().unwrap().trim())
                                        .collect();
                                    let mut end: Vec<&str> = grandchild
                                        .children()
                                        .skip_while(|p| p.tag_name().name() != "name")
                                        .skip(1)
                                        .take_while(|p| p.tag_name().name() != "comment")
                                        .filter(|p| p.text().is_some())
                                        .map(|p| p.text().unwrap().trim())
                                        .collect();
                                    start.append(&mut end);
                                    start.join(" ").trim().to_string()
                                };

                                let name = grandchild
                                    .children()
                                    .find(|x| x.tag_name().name() == "name")
                                    .unwrap()
                                    .text()
                                    .unwrap()
                                    .trim();

                                let comment = grandchild
                                    .children()
                                    .find(|x| x.tag_name().name() == "comment")
                                    .map(|x| x.text().unwrap());

                                let optional = grandchild
                                    .attribute("optional")
                                    .map(parse_bool)
                                    .unwrap_or(false);
                                let noautovalidity = grandchild
                                    .attribute("noautovalidity")
                                    .map(parse_bool)
                                    .unwrap_or(false);

                                let len = grandchild.attribute("len");
                                let values = grandchild.attribute("values");

                                let member = StructMember {
                                    name,
                                    r#type,
                                    comment,
                                    optional,
                                    noautovalidity,
                                    len,
                                    values,
                                };
                                members.push(member);
                            }
                            let returnedonly = child
                                .attribute("returnedonly")
                                .map(parse_bool)
                                .unwrap_or(false);
                            let structextends = child.attribute("structextends");
                            let r#struct = Type::Struct {
                                name,
                                members,
                                returnedonly,
                                structextends,
                            };
                            types.push(r#struct);
                        }
                        "union" => {
                            let mut members = Vec::new();
                            let comment = child.attribute("comment").unwrap();
                            for grandchild in child.children() {
                                if grandchild.children().count() < 2 {
                                    continue;
                                }

                                let r#type = {
                                    let mut start: Vec<&str> = grandchild
                                        .children()
                                        .take_while(|p| p.tag_name().name() != "name")
                                        .filter(|p| p.text().is_some())
                                        .map(|p| p.text().unwrap().trim())
                                        .collect();
                                    let mut end: Vec<&str> = grandchild
                                        .children()
                                        .skip_while(|p| p.tag_name().name() != "name")
                                        .skip(1)
                                        .take_while(|p| p.tag_name().name() != "comment")
                                        .filter(|p| p.text().is_some())
                                        .map(|p| p.text().unwrap().trim())
                                        .collect();
                                    start.append(&mut end);
                                    start.join(" ")
                                };
                                let name = grandchild
                                    .children()
                                    .find(|x| x.tag_name().name() == "name")
                                    .unwrap()
                                    .text()
                                    .unwrap();
                                let member = UnionMember { name, r#type };
                                members.push(member);
                            }
                            let r#union = Type::Union {
                                name,
                                members,
                                comment,
                            };
                            types.push(r#union);
                        }
                        _ => panic!("unimplemented type category"),
                    }
                }
            }
            _ => panic!("unimplemented tag"),
        }
    }

    trace!("END parse_types");

    types
}

fn parse_constants<'input>(node: Node<'input, 'input>) -> Vec<Type<'input>> {
    let mut constants = Vec::new();
    for child in node.children() {
        if child.has_attribute("alias") {
            let name = child.attribute("name").unwrap();
            let r#type = child.attribute("alias").unwrap();
            let alias = Type::Alias { name, r#type };
            constants.push(alias);
        } else if child.has_attribute("value") {
            let name = child.attribute("name").unwrap();
            let value = child.attribute("value").unwrap();
            let comment = child.attribute("comment");
            let constant = Type::Constant {
                name,
                value,
                comment,
            };
            constants.push(constant);
        }
    }
    constants
}

fn parse_enums<'input>(node: Node<'input, 'input>) -> Vec<Type<'input>> {
    let mut types = Vec::new();
    let mut values = Vec::new();
    let name = node.attribute("name").unwrap();
    let r#type = node.attribute("type").unwrap();
    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "enum" => {
                if child.has_attribute("value") {
                    let value = child.attribute("value").unwrap();
                    let value = if value.starts_with("0x") {
                        let value = value.trim_start_matches("0x");
                        isize::from_str_radix(value, 16).unwrap()
                    } else {
                        value.parse().unwrap()
                    };
                    let value = EnumerantValue {
                        name: child.attribute("name").unwrap(),
                        value,
                        comment: child.attribute("comment"),
                    };
                    values.push(value);
                } else if child.has_attribute("bitpos") {
                    let bitpos = child.attribute("bitpos").unwrap();
                    let value = if bitpos.starts_with("0x") {
                        let bitpos = bitpos.trim_start_matches("0x");
                        let bitpos = isize::from_str_radix(bitpos, 16).unwrap();
                        1 << bitpos
                    } else {
                        let bitpos: isize = bitpos.parse().unwrap();
                        1 << bitpos
                    };
                    let value = EnumerantValue {
                        name: child.attribute("name").unwrap(),
                        value,
                        comment: child.attribute("comment"),
                    };
                    values.push(value);
                } else if child.has_attribute("alias") {
                    let alias = Type::Alias {
                        name: child.attribute("name").unwrap(),
                        r#type: child.attribute("alias").unwrap(),
                    };
                    types.push(alias);
                } else {
                    panic!("unknown enum value")
                };
            }
            "unused" => {}
            _ => panic!("unimplemented enumeration"),
        }
    }
    let enumerant = Type::Enumerant {
        name,
        r#type,
        values,
    };
    types.push(enumerant);
    types
}

fn parse_commands<'input>(node: Node<'input, 'input>) -> Vec<Type<'input>> {
    let mut commands = Vec::new();
    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "command" => {
                if let Some(alias) = child.attribute("alias") {
                    let name = child.attribute("name").unwrap();
                    let alias = Type::Alias {
                        name,
                        r#type: alias,
                    };
                    commands.push(alias);
                } else {
                    let successcodes = child
                        .attribute("successcodes")
                        .unwrap_or("")
                        .split(',')
                        .filter(|&x| x != "")
                        .collect();
                    let errorcodes = child
                        .attribute("errorcodes")
                        .unwrap_or("")
                        .split(',')
                        .filter(|&x| x != "")
                        .collect();

                    let r#type = {
                        let mut start: Vec<&str> = child
                            .children()
                            .find(|x| x.tag_name().name() == "proto")
                            .unwrap()
                            .children()
                            .take_while(|p| p.tag_name().name() != "name")
                            .filter(|p| p.text().is_some())
                            .map(|p| p.text().unwrap().trim())
                            .collect();
                        let mut end: Vec<&str> = child
                            .children()
                            .skip_while(|p| p.tag_name().name() != "name")
                            .skip(1)
                            .filter(|p| p.text().is_some())
                            .map(|p| p.text().unwrap().trim())
                            .collect();
                        start.append(&mut end);
                        start.join(" ")
                    };

                    let name = child
                        .children()
                        .find(|x| x.tag_name().name() == "proto")
                        .unwrap()
                        .children()
                        .find(|x| x.tag_name().name() == "name")
                        .unwrap()
                        .text()
                        .unwrap();

                    // let rawstring: String = child
                    //     .children()
                    //     .find(|x| x.tag_name().name() == "proto")
                    //     .unwrap()
                    //     .children()
                    //     .filter_map(|x| x.text())
                    //     .flat_map(|x| x.chars())
                    //     .collect();
                    // let r#type = rawstring.rsplit(' ').nth(1).unwrap().trim().to_string();
                    // let name = rawstring.rsplit(' ').nth(0).unwrap().to_string();

                    let mut parameters = Vec::new();
                    for grandchild in child.children().filter(|x| x.tag_name().name() == "param") {
                        // let rawstring: String = grandchild
                        //     .children()
                        //     .filter_map(|x| x.text())
                        //     .flat_map(|x| x.chars())
                        //     .collect();
                        // let r#type = rawstring.rsplitn(2, ' ').nth(1).unwrap().trim().to_string();
                        // let name = rawstring.rsplitn(2, ' ').nth(0).unwrap().to_string();

                        let r#type = {
                            let mut start: Vec<&str> = grandchild
                                .children()
                                .take_while(|p| p.tag_name().name() != "name")
                                .filter_map(|p| p.text().map(str::trim))
                                .collect();
                            let mut end: Vec<&str> = grandchild
                                .children()
                                .skip_while(|p| p.tag_name().name() != "name")
                                .skip(1)
                                .filter_map(|p| p.text().map(str::trim))
                                .collect();
                            start.append(&mut end);
                            start.join(" ")
                        };

                        let name = grandchild
                            .children()
                            .find(|x| x.tag_name().name() == "name")
                            .unwrap()
                            .text()
                            .unwrap();

                        let optional =
                            parse_bool(grandchild.attribute("optional").unwrap_or("false"));
                        let externsync = grandchild.attribute("externsync");
                        let parameter = CommandParameter {
                            name,
                            r#type,
                            optional,
                            externsync,
                        };
                        parameters.push(parameter);
                    }
                    let command = Type::Command {
                        name,
                        r#type,
                        successcodes,
                        errorcodes,
                        parameters,
                    };
                    commands.push(command);
                }
            }
            _ => panic!("unimplemented command"),
        }
    }
    commands
}

fn parse_feature<'input>(node: Node<'input, 'input>) -> Feature<'input> {
    assert!(node.has_attribute("number"));
    let number = node.attribute("number").unwrap();
    let comment = node.attribute("comment");
    let mut requires = Vec::new();
    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "require" => {
                for grandchild in child.children() {
                    match grandchild.tag_name().name() {
                        "" | "comment" => {}
                        "type" => {
                            let v = FeatureRequire::Type { name: grandchild.attribute("name").unwrap() };
                            requires.push(v);
                        }
                        "enum" => {
                            let v = FeatureRequire::Enum { name: grandchild.attribute("name").unwrap() };
                            requires.push(v);
                        }
                        "command" => {
                            let v = FeatureRequire::Command { name: grandchild.attribute("name").unwrap() };
                            requires.push(v);
                        }
                        _ => panic!("unknown feature"),
                    }
                }
            }
            _ => panic!("unimplemented feature"),
        }
    }
    Feature {
        number,
        comment,
        requires,
    }
}

fn parse_extensions<'input>(node: Node<'input, 'input>) {
    for child in node.children() {
        match child.tag_name().name() {
            "" | "comment" => {}
            "extension" => {}
            _ => panic!("unimplemented extension"),
        }
    }
}

fn parse_bool(value: &str) -> bool {
    match value {
        "true" => true,
        "false" => false,

        // XXX: ???
        "true,false" | "false,true" => false,
        e => panic!("unknown bool type: {}", e),
    }
}
